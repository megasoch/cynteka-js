import { CyFormRendererDirective } from './directives/cy-form-renderer-directive';
import { CyTableRendererDirective } from './directives/cy-table-renderer-directive';

var moduleName = 'cynteka-js';

angular.module(moduleName, [])
    .directive('cyTableRenderer', CyTableRendererDirective.directiveFactory)
    .directive('cyFormRenderer', CyFormRendererDirective.directiveFactory);

export default moduleName;