/**
 * Created by alexander on 25.10.17.
 */

var communicationTypeListProvider =
    {
        ajax: true,
        data: [],
        ajaxConfig: {
            url: "http://localhost:8080/comtypes",
            processResults: function (data) {
                var id = 0;
                var data = data._embedded.comtypes.map(function (comtype) {
                    return {id: ++id, text: comtype.name}
                });
                return {
                    results: data
                }
            }
        }
    };

var personListProvider =
    {
        ajax: true,
        data: [],
        ajaxConfig: {
            url: "http://localhost:8080/persons",
            processResults: function (data) {
                var id = 0;
                var data = data._embedded.persons.map(function (item) {
                    return {id: ++id, text: item.lastName + " " + item.firstName}
                });
                return {
                    results: data
                }
            }
        }
    };


function generateFormConfig(tableConfig, formAdditionalConfig, isFilter) {
    var formConfig = [];
    tableConfig.forEach(function (configItem) {
        if ((configItem.filter && isFilter) || (configItem.form && !isFilter)) {
            if (configItem.type === "date") {
                if (configItem.form) {
                    formConfig.push(
                        {
                            field: configItem.fields[0],
                            type: configItem.type,
                            label: configItem.formNames[0],
                            classNames: configItem.classNames,
                            row: configItem.row,
                            formId: configItem.formId,
                            formField: configItem.formField,
                            required: isFilter ? false : configItem.required
                        }
                    );
                } else {
                    formConfig.push(
                        {
                            field: configItem.fields[0] + "From",
                            type: configItem.type,
                            label: configItem.formNames[0] + " от",
                            classNames: configItem.classNames,
                            row: configItem.row,
                            formId: configItem.formId + "From",
                            formField: configItem.formField + "From",
                            required: isFilter ? false : configItem.required
                        }
                    );
                    formConfig.push(
                        {
                            field: configItem.fields[0] + "To",
                            type: configItem.type,
                            label: configItem.formNames[0] + " до",
                            classNames: configItem.classNames,
                            row: configItem.row,
                            formId: configItem.formId + "To",
                            formField: configItem.formField + "To",
                            required: isFilter ? false : configItem.required
                        }
                    );
                }
            } else if (configItem.type === "select2") {
                formConfig.push(
                    {
                        field: configItem.fields[0],
                        type: configItem.type,
                        label: configItem.formNames[0],
                        classNames: configItem.classNames,
                        row: configItem.row,
                        formId: configItem.formId,
                        formField: configItem.formField,
                        listProvider: configItem.listProvider,
                        required: isFilter ? false : configItem.required
                    }
                );
            }else if (configItem.type === "datetimepicker") {
                formConfig.push(
                    {
                        field: configItem.fields[0],
                        type: configItem.type,
                        label: configItem.formNames[0],
                        classNames: configItem.classNames,
                        row: configItem.row,
                        formId: configItem.formId,
                        formField: configItem.formField,
                        required: isFilter ? false : configItem.required
                    }
                );
            } else if (configItem.type === "uiselect") {
                formConfig.push(
                    {
                        fields: configItem.fields,
                        type: configItem.type,
                        label: configItem.formNames[0],
                        classNames: configItem.classNames,
                        row: configItem.row,
                        formId: configItem.formId,
                        formField: configItem.formField,
                        listProvider: configItem.listProvider,
                        required: isFilter ? false : configItem.required
                    }
                );
            } else if (configItem.fields.length > 1) {
                configItem.fields.forEach(function (field, index) {
                    formConfig.push(
                        {
                            field: field,
                            type: configItem.type,
                            label: configItem.formNames[index],
                            classNames: configItem.classNames,
                            row: configItem.row,
                            formField: configItem.formField,
                            required: isFilter ? false : configItem.required
                        }
                    );
                });
            } else {
                formConfig.push(
                    {
                        field: configItem.fields[0],
                        type: configItem.type,
                        label: configItem.formNames[0],
                        classNames: configItem.classNames,
                        row: configItem.row,
                        formField: configItem.formField,
                        required: isFilter ? false : configItem.required
                    }
                );
            }
        }
    });
    return formConfig;
};

function configPostprocessor(config) {
    config.forEach(function (configItem) {
        if (configItem.type === "date") {
            $("#" + configItem.formId).datetimepicker();
        } else if (configItem.type === "select2") {
            if (configItem.listProvider.ajax) {
                $("#" + configItem.formId).select2({
                    ajax: configItem.listProvider.ajaxConfig,
                    minimumResultsForSearch: -1
                });
            } else {
                $("#" + configItem.formId).select2({
                    data: configItem.listProvider.data
                });
            }
        }
    });
};

function fromConfigToModel(configItem) {
    if (configItem.type === "select2") {
        var select2Selection = $("#" + configItem.formId).select2("data");
        if (select2Selection.length > 0) {
            return {id: select2Selection[0].id};
        } else {
            return null;
        }
    } else if (configItem.type === "date") {
        var date = $("#" + configItem.formId).data("DateTimePicker").date();
        if (date != null && date != undefined) {
            return date.valueOf();
        } else {
            return null;
        }
    }
    return null;
};

function prepareTaskToSave(configItem, model) {
    if (configItem.type === "uiselect") {
        if (model != null && model != undefined) {
            return {id: model.id};
        } else {
            return null;
        }
    } else if (configItem.type === "datetimepicker") {
        if (model != null && model != undefined) {
            return model.valueOf();
        } else {
            return null;
        }
    }
    return null;
}

export {personListProvider, communicationTypeListProvider, uiSelectListProvider, generateFormConfig, configPostprocessor, fromConfigToModel, prepareTaskToSave}
