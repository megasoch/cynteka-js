/**
 * Created by alexander on 02.11.17.
 */

/*****BEGIN***** Шаблоны для элементов формы *****BEGIN*****/

var stringTemplate = function (label, formModelName, formName, formField, required) {
    var template =
        "<div class=\"form-group\" ng-class=\"{ 'has-error': " + formName + "." + formField + ".$invalid }\">" +
        "<label>" + label + ":</label>" +
        "<input type=\"text\" class=\"form-control\" name=\"" + formField + "\" ng-model=\"" + formModelName + "." + formField + "\" ng-required=\"" + required + "\"/>" +
        "</div>";
    return template;
}

var dateTimePickerTemplate = function (label, formId, formField, formModelName, formName, required) {
    var template =
        "<div class=\"form-group\">" +
        "<label>" + label + ":</label>" +
        "<div class=\"input-group date\" id=\"" + formId + "\">" +
        "<input name=\"" + formField + "\" type=\"text\" class=\"form-control\" ng-model=\"" + formModelName + "." + formField + "\" ng-required=\"" + required + "\"/>" +
        "<span class=\"input-group-addon\">" +
        "<span class=\"glyphicon glyphicon-calendar\"></span>" +
        "</span>" +
        "</div>" +
        "</div>";
        // "<p ng-if=\"!" + formName + "." + formField + ".$valid\">Это поле должно быть заполнено.</p>";
    return template;
}

var select2Template = function (label, formId) {
    var template =
        "<div class=\"form-group\">" +
        "<label>" + label + ":</label>" +
        "<select class=\"form-control\" id=\"" + formId + "\">" +
        "</select>" +
        "</div>";
    return template;
}

var textareaTemplate = function (label, formField, formModelName, formName, required) {
    var template =
        "<div class=\"form-group\" ng-class=\"{ 'has-error': " + formName + "." + formField + ".$invalid }\">" +
        "<label>" + label + ":</label>" +
        "<textarea name=\"" + formField + "\" type=\"text\" class=\"form-control\" ng-model=\"" + formModelName + "." + formField + "\" ng-required=\"" + required + "\"></textarea>" +
        "</div>";
        // "<p ng-if=\"!" + formName + "." + formField + ".$valid\">Это поле должно быть заполнено.</p>";
    return template;
}

var angularDateTimePickerTemplate = function (label, formField, formModelName, formName, required) {
    var template =
        "<div class=\"form-group\" ng-class=\"{ 'has-error': " + formName + "." + formField + ".$invalid }\">" +
        "<label>" + label + ":</label>" +
        "<div class=\"input-group\">" +
        "<input class=\"form-control\" name=\"" + formField + "\" datetimepicker ng-model=\"" + formModelName + "." + formField + "\" ng-required=\"" + required + "\"/>" +
        "<span class=\"input-group-addon\">" +
        "<span class=\"glyphicon glyphicon-calendar\"></span>" +
        "</span>" +
        "</div>" +
        "</div>";
        // "<div class=\"form-group\">" +
        // "<label>" + label + ":</label>" +
        // "<div class=\"input-group\" datetimepicker ng-model=\"" + formModelName + "." + formField + "\" ng-required=\"" + required + "\">" +
        // "<input class=\"form-control\"/>" +
        // "<span class=\"input-group-addon\">" +
        //     "<span class=\"glyphicon glyphicon-calendar\"></span>" +
        // "</span>" +
        // "</div>" +
        // "</div>";
    return template;
}

var uiSelectTemplate = function (label, formField, formModelName, formName, index, required) {
    var template =
        "<div class=\"form-group\" ng-class=\"{ 'has-error': " + formName + "." + formField + ".$invalid }\">" +
        "<label>" + label + ":</label>" +
        "<ui-select name=\"" + formField + "\" ng-model=\"" + formModelName + "." + formField + "\" theme=\"bootstrap\" ng-required=\"" + required + "\">" +
        "<ui-select-match>{{$select.selected.text}}</ui-select-match>" +
        "<ui-select-choices repeat=\"item in config[" + index + "].listProvider.data | filter: $select.search\">" +
        "<span ng-bind-html=\"item.text | highlight: $select.search\"></span>" +
        "</ui-select-choices>" +
        "</ui-select>" +
        "</div>";
    return template;
}

/*****END***** Шаблоны для элементов формы *****END*****/

/********** Функция генерации шаблона формы **********/
function generateFormTemplate(config, formName, formModelName, submitFunction, submitButtonText) {
    var template =
    "<form ng-submit=\"" + submitFunction + "\" name=\"" + formName + "\">";
    var prevRow = -1;
        template += "<div class=\"row\">";
        config.forEach(function (configItem, cfgIndex) {
            // if (prevRow != configItem.row) {
            //     if (prevRow != -1) {
            //         template += "</div>";
            //     }
            //     template += "<div class=\"row\">";
            // }
            template += "<div class=\"" + configItem.classNames.join(" ") + "\">";
            if (configItem.type === "string") {
                template += stringTemplate(configItem.label, formModelName, formName, configItem.formField, configItem.required);
            } else if (configItem.type === "date") {
                template += dateTimePickerTemplate(configItem.label, configItem.formId, configItem.formField, formModelName, formName, configItem.required);
            } else if (configItem.type === "select2") {
                template += select2Template(configItem.label, configItem.formId);
            } else if (configItem.type === "textarea") {
                template += textareaTemplate(configItem.label, configItem.formField, formModelName, formName, configItem.required);
            } else if (configItem.type === "datetimepicker") {
                template += angularDateTimePickerTemplate(configItem.label, configItem.formField, formModelName, formName, configItem.required);
            } else if (configItem.type === "uiselect") {
                template += uiSelectTemplate(configItem.label, configItem.formField, formModelName, formName, cfgIndex, configItem.required);
            }
            template += "</div>";
            prevRow = configItem.row;
        });
        template += "</div>";
        template += "<div class=\"btn-group\">";
        template += "<button class=\"btn btn-success\" type=\"submit\" ng-disabled=\"" + formName + ".$invalid\">" + submitButtonText + "</button>";
        template += "<button class=\"btn btn\" type=\"button\" ng-click=\"clear()\">Очистить</button>";
        template += "</div>";
    template +=
    "</form>";
    return template;
}

/********** Функция генерации шаблона таблицы **********/
function generateTableTemplate(config, data, scope) {
    var template =
    "<table class=\"table table-bordered\">" +
        "<tr>" +
            "<th ng-repeat=\"th in config\" ng-click=\"order(th.orderField)\">{{th.title}}</th>" +
        "</tr>" +
        "<tr ng-repeat=\"tr in data\">";
    template +=
            "<td ng-repeat=\"td in config\">" +
                "<div ng-if=\"!isTemplate(td.renderer)\" ng-bind-html=\"td.renderer(tr, td) | toTrusted\"></div>" +
                "<div ng-if=\"isTemplate(td.renderer)\" ng-include=\"td.renderer\"></div>" +
            "</td>";
    template +=
        "</tr>" +
    "</table>";
    template +=
        "<div class=\"text-center\">" +
        "<button type=\"button\" class=\"btn btn-success\" ng-click=\"loadMore()\">Загрузить еще</button>" +
        "</div>";
    return template;
}

export {stringTemplate, dateTimePickerTemplate, select2Template, textareaTemplate, angularDateTimePickerTemplate, uiSelectTemplate, generateFormTemplate, generateTableTemplate}