/**
 * Created by alexander on 09.11.17.
 */

import {generateFormTemplate} from './../templates/formTemplates';

class CyFormRendererDirective {
    /*@ngInject*/
    constructor($compile) {
        this.restrict = 'E';
        this.scope = {
            config: '=',

            editedObject: '=',

            formName: '@',
            formModelName: '@',

            submitButtonText: '@',
            submitFunction: '=',
            submitFunctionName: '@',

            updateButtonText: '@',
            updateFunction: '=',
            updateFunctionName: '@',

            filterFunction: '='
        }

        this.$compile = $compile;
    }

    link(scope, element) {
        var submitFunction = scope.submitFunctionName;
        var submitButtonText = scope.submitButtonText;
        if (scope.editedObject != null) {
            scope[scope.formModelName] = scope.editedObject;
            submitFunction = scope.updateFunctionName;
            submitButtonText = scope.updateButtonText;
        } else {
            scope[scope.formModelName] = {};
        }
        var template = generateFormTemplate(scope.config, scope.formName, scope.formModelName, submitFunction, submitButtonText);
        var linkFn = this.$compile(template);
        var content = linkFn(scope);
        element.replaceWith(content);
        // configPostprocessor(scope.config);

        scope.submit = function() {
            scope.submitFunction(scope.config, scope[scope.formModelName]);
        }

        scope.update = function() {
            scope.updateFunction(scope.config, scope[scope.formModelName]);
        }

        scope.filter = function () {
            scope.filterFunction(scope[scope.formModelName]);
        }

        scope.clear = function () {
            scope[scope.formModelName] = {};
        }
    }

    static directiveFactory($compile) {
        CyFormRendererDirective.instance = new CyFormRendererDirective($compile);
        return CyFormRendererDirective.instance;
    }
}

CyFormRendererDirective.directiveFactory.$inject = ['$compile', '$location'];

export {CyFormRendererDirective}