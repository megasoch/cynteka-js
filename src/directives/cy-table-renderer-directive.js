/**
 * Created by alexander on 10.11.17.
 */

// import {generateTableTemplate} from './../templates/formTemplates'

class CyTableRendererDirective {
    /*@ngInject*/
    constructor($compile, $filter) {
        this.restrict = 'E';
        this.scope = {
            config: '=',

            getdata: '=',
            type: '@',

            filter: '=',
            filterFunction: '=',

            editFunction: '=',
            deleteFunction: '='
        }

        this.$compile = $compile;
        this.$filter = $filter;
        this.template = require('./cy-table-renderer-template.html');
    }

    link(scope, element) {
        //Индекс подгузки данных в таблицу
        scope.startIndex = 0;
        //Количество подгружаемых элементов
        scope.limit = 5;
        //Начальная инициализация
        scope.data = [];
        //Функция проверки как рисуется шаблон(функция или html)
        scope.isTemplate = function (renderer) {
            return typeof renderer === 'string';
        };

        //Функция для возможности отображения к полям объекта как: object["field1.field2"]
        scope.resolve = function(path, obj) {
            return path.split('.').reduce(function(prev, curr) {
                return prev ? prev[curr] : null
            }, obj || self)
        };

        //Конфигурация таблицы
        var config = scope.config;
        //Данные для таблицы или функция возвращающая Promise
        var promise = scope.getdata();
        promise.then(result => {
            var concatenatedData = [];
            concatenatedData = scope.filterFunction(result.data[scope.type], scope.filter);
            scope.data = scope.data.concat(concatenatedData.slice(scope.startIndex, scope.startIndex + scope.limit));
            scope.startIndex += scope.limit;
            // scope.data = this.$filter('orderBy')(scope.data, []);
            // var template = generateTableTemplate(config, scope.data, scope);
            // var linkFn = this.$compile(template);
            // var content = linkFn(scope);
            // element.replaceWith(content);
            scope.$apply();
        });

        scope.order = function (field) {
            if (!scope.orderArray) {
                scope.orderArray = [field];
            } else {
                if (scope.orderArray[0] == field) {
                    scope.orderArray[0] = "-" + field;
                } else {
                    scope.orderArray[0] = field;
                }
            }
            // scope.data = this.$filter('orderBy')(scope.data, scope.orderArray);
        };

        scope.loadMore = function () {
            var promise = scope.getdata();
            promise.then(result => {
                var concatenatedData = [];
                concatenatedData = scope.filterFunction(result.data[scope.type], scope.filter);
                scope.data = scope.data.concat(concatenatedData.slice(scope.startIndex, scope.startIndex + scope.limit));
                scope.startIndex += scope.limit;
                // scope.data = this.$filter('orderBy')(scope.data, []);
                scope.$apply();
            });
        }

        scope.delete = function(object) {
            scope.deleteFunction(scope, object);
        }

        scope.edit = function(object) {
            scope.editFunction(object);
        }
    }

    static directiveFactory($compile, $filter) {
        CyTableRendererDirective.instance = new CyTableRendererDirective($compile, $filter);
        return CyTableRendererDirective.instance;
    }
}

CyTableRendererDirective.directiveFactory.$inject = ['$compile', '$filter']

export {CyTableRendererDirective}